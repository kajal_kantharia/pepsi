﻿$(function () {

    $(".numbers-row").append('<div class="inc qty-button"><span><img src="../../shop/assets/plus-icon.png" /></span></div>');
    $(".numbers-row").prepend('<div class="dec qty-button"><span><img src="../../shop/assets/minus-icon.png" /></span></div>');

    $(".qty-button").on("click", function () {

        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.parent().find("input").val(newVal);

    });

});