﻿function initTagImages() {
    var imgDefer = $('img');
    for (var i = 0; i < imgDefer.length; i++) {
        if (imgDefer[i].getAttribute('data-img')) {
            imgDefer[i].setAttribute('src', imgDefer[i].getAttribute('data-img'));
        }
    }
}

function ajaxImages(parentClass) {
    var ajaximgDefer = parentClass.find('img');
    //console.log(ajaximgDefer);
    for (var i = 0; i < ajaximgDefer.length; i++) {
        if (ajaximgDefer[i].getAttribute('data-img')) {
            ajaximgDefer[i].setAttribute('src', ajaximgDefer[i].getAttribute('data-img'));
        }
    }
}

function initBgImages() {
    var bgimgDefer = $('.bg-trick');
    for (var i = 0; i < bgimgDefer.length; i++) {
        if (bgimgDefer[i].getAttribute('data-style')) {
            bgimgDefer[i].setAttribute('style', bgimgDefer[i].getAttribute('data-style'));
        }
    }
}

//$(document).ready(function () {
//    var test = $('.local');
//    ajaxImages(test);
//});


window.onload = function () {
    setTimeout(function () {
        initTagImages();
        initBgImages();
    }, 5000);
}

