﻿var themeName = {

    init: function () {
        this.bxSider();
        this.meanMenu();
        this.smoothScroll();
        this.slickCall();
        this.closePopup();
        this.openPopup();
        //this.dropdown();
        //this.tabination();
        //this.heapBox();
        //this.toolTip();

    },

    smoothScroll: function () {
        $('.menu-list li a').click(function () {
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 1000);

            return false;
        });
    },

    openPopup: function () {
        $('.ingredients-wrapper').click(function () {
            $('.popup').fadeIn();
        });
    },

    //popup toggle jquery
    closePopup: function () {
        $('.pop-up-close,.overlay-close').click(function () {
            $(this).parents('.popup').fadeOut();
        });
    },

    meanMenu: function () {
        jQuery('header nav').meanmenu();
    },

    //home page banner slider start 
    bxSider: function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            captions: true,
            auto: false
        });
    },
    //home page banner slider end 

    //common dropdown start
    dropdown: function () {

        $('select.custom-select').each(function () {

            var $this = $(this), numberOfOptions = $(this).children('option').length;

            $this.addClass('select-hidden');
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');

            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option').eq(0).text());

            var $list = $('<ul />', {
                'class': 'select-options'
            }).insertAfter($styledSelect);

            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }

            var $listItems = $list.children('li');

            $styledSelect.click(function (e) {
                e.stopPropagation();
                $('div.select-styled.active').not(this).each(function () {
                    $(this).removeClass('active').next('ul.select-options').slideUp();
                });
                $(this).toggleClass('active').next('ul.select-options').toggle();
            });

            $listItems.click(function (e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                //console.log($this.val());
            });

            $(document).click(function () {
                $styledSelect.removeClass('active');
                $list.hide();
            });

        });
    },
    //common dropdown end

    heapBox: function () {
        $(".basic-example").heapbox({
            'effect': {
                'type': 'fade',
                'speed': 100
            },
            'openStart': function () {
                console.log('beforeStart');
            },
            'closeComplete': function () { console.log('afterclose'); },
        });
    },

    slickCall: function () {
        $(".package").slick({
            dots: false,
            arrows: true,
            autoplay: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                 {
                     breakpoint: 1199,
                     settings: {
                         slidesToShow: 2,
                         slidesToScroll: 1
                     }
                 },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
            ]
        });

        $(".nutrition-slider").slick({
            dots: false,
            arrows: true,
            autoplay: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
            ]
        });

        $(".testimonial").slick({
            dots: false,
            autoplay: false,
            arrows: false,
            infinite:false,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                  {
                      breakpoint: 992,
                      settings: {
                          slidesToShow: 2,
                          arrows: true,
                      }
                  },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        arrows: true,
                    }
                },
            ]
        });

        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 5,
            arrows: false,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            responsive: [
                 {
                     breakpoint: 1200,
                     settings: {
                         slidesToShow: 3,
                     }
                 },
              {
                  breakpoint: 992,
                  settings: {
                      slidesToShow: 3,
                  }
              },
               {
                   breakpoint: 768,
                   settings: {
                       slidesToShow: 3,
                   }
               },
            ]
        });


    },

    tabination: function () {
        var $tabs = $('#horizontalTab');
        $tabs.responsiveTabs({
            rotate: false,
            startCollapsed: 'accordion',
            collapsible: 'accordion',
            setHash: true,
        });
    },

    toolTip: function () {
        $(".tooltiptext").asTooltip({
            show: {
                target: '.tooltiptext'
            },
            hide: {
                target: '.tooltiptext'
            }
        });

        $(".tooltiphtml").asTooltip({
            show: { event: 'click' },
            hide: { event: 'click' },
            content: '<a href="#">helllo</a>',
        });

    },

}

$(document).ready(function () {

    var winWidth = $(window).width();
    var winHeight = $(window).outerHeight();
    var winScroll;

    themeName.init();

});

$(window).load(function () {
    $('.loader-wrapper').fadeOut();
});

// Hide Header on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var headerHeight = $('.while-scroll-header').outerHeight();

$(window).scroll(function (e) {
    winScroll = $(window).scrollTop();
    didScroll = true;

    afterScrollHeader();
});

function afterScrollHeader() {
    if (winScroll > 250) {
        $('.after-scroll-header').addClass('in-view');
    }
    else {
        $('.after-scroll-header').removeClass('in-view');
    }
}

setInterval(function () {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > headerHeight) {
        // Scroll Down
        $('.while-scroll-header').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
            $('.while-scroll-header').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}